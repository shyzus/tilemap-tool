package org.shydev.utility.tilemaptool.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoordinateTest {

    private Coordinate coordinate;

    @BeforeEach
    void setUp() {
        coordinate = new Coordinate(0,0,0);
    }

    @Test
    void getX() {
        // Assert
        assertEquals(0, coordinate.getX());
    }

    @Test
    void setX() {
        // Arrange
        int newX = 5;

        // Act
        coordinate.setX(newX);

        // Assert
        assertEquals(newX, coordinate.getX());
    }

    @Test
    void getY() {
        // Assert
        assertEquals(0, coordinate.getY());
    }

    @Test
    void setY() {
        // Arrange
        int newY = 5;

        // Act
        coordinate.setY(newY);

        // Assert
        assertEquals(newY, coordinate.getY());
    }

    @Test
    void getZ() {
        // Assert
        assertEquals(0, coordinate.getZ());
    }

    @Test
    void setZ() {
        // Arrange
        int newZ = 5;

        // Act
        coordinate.setZ(newZ);

        // Assert
        assertEquals(newZ, coordinate.getZ());
    }

    @Test
    void setCoordinate() {
        // Arrange
        int newX = 12;
        int newY = 15;
        int newZ = 22;

        // Act
        coordinate.setCoordinate(12, 15, 22);

        // Assert
        assertEquals(newX, coordinate.getX());
        assertEquals(newY, coordinate.getY());
        assertEquals(newZ, coordinate.getZ());
    }

    @Test
    void testToString() {
        // Arrange
        int newX = 12;
        int newY = 15;
        int newZ = 22;
        coordinate.setCoordinate(newX, newY, newZ);

        // Assert
        assertEquals("Coordinate{" +
                "x=" + newX +
                ", y=" + newY +
                ", z=" + newZ +
                '}',coordinate.toString());
    }

    @Test
    void testNotEquals() {
        // Arrange
        int newX = 12;
        int newY = 15;
        int newZ = 22;
        Coordinate newCoordinate = new Coordinate(newX, newY, newZ);

        // Act
        boolean isEqual = newCoordinate.equals(coordinate);

        // Assert
        assertFalse(isEqual);
    }

    @Test
    void testNotCoordinate() {
        // Arrange
        String newCoordinate = "newCoordinate";

        // Act
        boolean isEqual = coordinate.equals(newCoordinate);

        // Assert
        assertFalse(isEqual);
    }

    @Test
    void testEquals() {
        // Arrange
        int newX = 12;
        int newY = 15;
        int newZ = 22;
        Coordinate newCoordinate = new Coordinate(newX, newY, newZ);
        coordinate.setCoordinate(newX, newY, newZ);

        // Act
        boolean isEqual = newCoordinate.equals(coordinate);

        // Assert
        assertTrue(isEqual);
    }
}