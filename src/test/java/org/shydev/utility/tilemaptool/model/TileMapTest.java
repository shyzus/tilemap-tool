package org.shydev.utility.tilemaptool.model;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.shydev.utility.tilemaptool.service.JSONService;
import org.shydev.utility.tilemaptool.service.TileMapService;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TileMapTest {

    private final JSONService JSON_SERVICE = new JSONService();
    private TileMap tileMap;

    @BeforeEach
    void setUp() throws FileNotFoundException {
        tileMap = (TileMap) JSON_SERVICE.readFromFileToObject(getClass().getResource("/test-tilemap.json").getPath(), TileMap.class);
        TileMapService.setLoadedTileMap(tileMap);
    }

    @AfterEach
    void tearDown() {
        tileMap = null;
        TileMapService.setLoadedTileMap(null);
    }

    @Test
    void addTileMapToPane() {
        // Arrange
        Pane pane = new Pane();
        tileMap.init();

        // Act
        tileMap.addTileMapToPane(pane);

        // Assert
        for (Layer layer : tileMap.getLayerList()) {
            Assertions.assertTrue(pane.getChildren().contains(layer.getGridPane()));
        }

    }

    @Test
    void getLayerByNameCorrect() {
        // Arrange
        String name = tileMap.getLayerList().get(0).getName();

        // Act
        Layer result = tileMap.getLayerByName(name);

        // Assert
        Assertions.assertEquals(tileMap.getLayerList().get(0), result);
    }

    @Test
    void getLayerByNameIncorrect() {
        // Arrange
        String name = "blah";

        // Act
        Layer result = tileMap.getLayerByName(name);

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void addLayerWithGridpaneToPane() {
        // Arrange
        Pane pane = new Pane();
        Layer layer = new Layer();
        Tile tile = new Tile(Color.AQUA, 32);
        GridPane gridPane = new GridPane();
        gridPane.add(tile.getRectangle(), tile.getCoordinateX(), tile.getCoordinateY());
        layer.setGridPane(gridPane);
        layer.addTile(tile);

        // Act
        tileMap.addLayerToPane(layer, pane);

        // Assert
        Assertions.assertTrue(tileMap.getLayerList().contains(layer));
        Assertions.assertTrue(pane.getChildren().contains(gridPane));
    }

    @Test
    void addLayerWithoutGridpaneToPane() {
        // Arrange
        Pane pane = new Pane();
        Layer layer = new Layer();
        Tile tile = new Tile(Color.AQUA, 32);
        layer.addTile(tile);

        // Act
        tileMap.addLayerToPane(layer, pane);

        // Assert
        Assertions.assertTrue(tileMap.getLayerList().contains(layer));
        Assertions.assertTrue(pane.getChildren().contains(layer.getGridPane()));
    }

    @Test
    void removeLayerFromPane() {
        // Arrange
        Layer selectedLayer = tileMap.getLayerList().get(0);
        selectedLayer.displayLayer();
        Pane pane = new Pane();
        pane.getChildren().add(selectedLayer.getGridPane());

        // Act
        tileMap.removeLayerFromPane(selectedLayer, pane);

        // Assert
        Assertions.assertFalse(tileMap.getLayerList().contains(selectedLayer));
        Assertions.assertFalse(pane.getChildren().contains(selectedLayer.getGridPane()));
    }

    @Test
    void setTopLayerWithGridPane() {
        // Arrange
        GridPane gridPane = new GridPane();
        Layer layer = tileMap.getTopLayer();
        layer.setGridPane(gridPane);

        // Act
        tileMap.setTopLayer(layer);

        // Assert
        Assertions.assertEquals(layer, tileMap.getTopLayer());
    }

    @Test
    void addColumn() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        tileMap.init();

        // Act
        tileMap.addColumn(tiles);

        // Assert
        tileMap.getLayerList().forEach(layer -> {
            for (Tile tile : tiles) {
                Assertions.assertTrue(layer.getTiles().contains(tile));
            }
        });
    }

    @Test
    void addRow() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        tileMap.init();

        // Act
        tileMap.addRow(tiles);

        // Assert
        tileMap.getLayerList().forEach(layer -> {
            for (Tile tile : tiles) {
                Assertions.assertTrue(layer.getTiles().contains(tile));
            }
        });
    }

    @Test
    void removeColumn() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        tileMap.init();
        int oldColumnCount = tileMap.getTopLayer().getColumnCount();

        // Act
        tileMap.removeColumn(oldColumnCount - 1);

        // Assert
        tileMap.getLayerList().forEach(layer -> {
            for (Tile tile : tiles) {
                Assertions.assertFalse(layer.getTiles().contains(tile));
            }
        });
    }

    @Test
    void removeRow() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        tileMap.init();
        int oldRowCount = tileMap.getTopLayer().getRowCount();

        // Act
        tileMap.removeRow(oldRowCount - 1);

        // Assert
        tileMap.getLayerList().forEach(layer -> {
            for (Tile tile : tiles) {
                Assertions.assertFalse(layer.getTiles().contains(tile));
            }
        });
    }

    @Test
    void getLayerNames() {
        // Arrange
        tileMap.init();
        String[] layerNames = new String[tileMap.getLayerList().size()];
        for (int i = 0; i < tileMap.getLayerList().size(); i++) {
            layerNames[i] = tileMap.getLayerList().get(i).getName();
        }

        // Act
        String[] result = tileMap.getLayerNames();

        // Assert
        for (int i = 0; i < layerNames.length; i++) {
            Assertions.assertEquals(layerNames[i], result[i]);
        }

    }

    @Test
    void toggleGrid() {
        // Arrange
        tileMap.init();
        boolean oldIsVisible = tileMap.getTopLayer().getGridPane().isGridLinesVisible();

        // Act
        tileMap.toggleGrid();

        // Assert
        for (Layer layer : tileMap.getLayerList()) {
            Assertions.assertEquals(!oldIsVisible, layer.getGridPane().isGridLinesVisible());
        }
    }

    @Test
    void setBase64Tiles() {
        // Arrange
        ArrayList<String> base64 = new ArrayList<>();
        base64.add("1");
        base64.add("2");

        // Act
        tileMap.setBase64Tiles(base64);

        // Assert
        Assertions.assertEquals(base64, tileMap.getBase64Tiles());
    }
}