package org.shydev.utility.tilemaptool.model;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.shydev.utility.tilemaptool.service.JSONService;
import org.shydev.utility.tilemaptool.service.TileMapService;

import java.io.FileNotFoundException;

class TileTest {

    private final JSONService JSON_SERVICE = new JSONService();
    private Tile tile;
    private final int TILE_SIZE = 32;
    private final String TILE_NAME = "TEST-TILE";

    @BeforeEach
    void setUp() {
        tile = null;
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void testPaintConstructor() {
        // Arrange
        Paint paint = Color.AQUA;

        // Act
        tile = new Tile(paint, TILE_SIZE);

        // Assert
        Assertions.assertNotNull(tile);
        Assertions.assertEquals(tile.getBaseColor(), paint);
        Assertions.assertNotNull(tile.getRectangle());
        Assertions.assertNotNull(tile.getCoordinate());
    }

    @Test
    void testPaintAndNameConstructor() {
        // Arrange
        Paint paint = Color.AQUA;

        // Act
        tile = new Tile(paint, TILE_NAME, TILE_SIZE);

        // Assert
        Assertions.assertNotNull(tile);
        Assertions.assertEquals(tile.getBaseColor(), paint);
        Assertions.assertEquals(tile.getName(), TILE_NAME);
        Assertions.assertNotNull(tile.getRectangle());
        Assertions.assertNotNull(tile.getCoordinate());
    }

    @Test
    void testIndexConstructor() throws FileNotFoundException {
        // Arrange
        TileMap tileMap = (TileMap) JSON_SERVICE.readFromFileToObject(getClass().getResource("/test-tilemap.json").getPath(), TileMap.class);
        TileMapService.setLoadedTileMap(tileMap);
        int index = 0;

        // Act
        tile = new Tile(index, TILE_SIZE);

        // Assert
        Assertions.assertNotNull(tile);
        Assertions.assertEquals(tile.getbase64Index(), index);
        Assertions.assertNotNull(tile.getRectangle());
        Assertions.assertNotNull(tile.getCoordinate());
    }

    @Test
    void testIndexAndNameConstructor() throws FileNotFoundException {
        // Arrange
        TileMap tileMap = (TileMap) JSON_SERVICE.readFromFileToObject(getClass().getResource("/test-tilemap.json").getPath(), TileMap.class);
        TileMapService.setLoadedTileMap(tileMap);
        int index = 0;

        // Act
        tile = new Tile(index, TILE_NAME, TILE_SIZE);

        // Assert
        Assertions.assertNotNull(tile);
        Assertions.assertEquals(tile.getbase64Index(), index);
        Assertions.assertEquals(tile.getName(), TILE_NAME);
        Assertions.assertNotNull(tile.getRectangle());
        Assertions.assertNotNull(tile.getCoordinate());
    }

    @Test
    void testInitNoColorHex() throws FileNotFoundException {
        // Arrange
        TileMap tileMap = (TileMap) JSON_SERVICE.readFromFileToObject(getClass().getResource("/test-tilemap.json").getPath(), TileMap.class);
        TileMapService.setLoadedTileMap(tileMap);
        tile = new Tile();
        tile.setBase64Index(0);

        // Act
        tile.init();

        // Assert
        Assertions.assertNotNull(tile.getRectangle());

    }

    @Test
    void testInitNoRectangle() throws FileNotFoundException {
        // Arrange
        TileMap tileMap = (TileMap) JSON_SERVICE.readFromFileToObject(getClass().getResource("/test-tilemap.json").getPath(), TileMap.class);
        TileMapService.setLoadedTileMap(tileMap);
        tile = new Tile();
        tile.setColorHex(Color.AQUA.toString());

        // Act
        tile.init();

        // Assert
        Assertions.assertEquals(Color.AQUA.toString(), tile.getColorHex());
        Assertions.assertEquals(Color.AQUA, tile.getBaseColor());
        Assertions.assertNotNull(tile.getRectangle());

    }

    @Test
    void recreateRectangleNoBaseColor() throws FileNotFoundException {
        // Arrange
        TileMap tileMap = (TileMap) JSON_SERVICE.readFromFileToObject(getClass().getResource("/test-tilemap.json").getPath(), TileMap.class);
        TileMapService.setLoadedTileMap(tileMap);
        tile = new Tile();
        tile.setBase64Index(0);

        // Act
        Rectangle result = tile.recreateRectangle();

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, tile.getRectangle());
    }

    @Test
    void recreateRectangleWithBaseColor() {
        // Arrange
        tile = new Tile();
        tile.setBaseColor(Color.AQUA);

        // Act
        Rectangle result = tile.recreateRectangle();

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, tile.getRectangle());
    }



    @Test
    void setCoordinateObject() {
        // Arrange
        tile = new Tile();
        Coordinate coordinate = new Coordinate(4,2,1);

        // Act
        tile.setCoordinate(coordinate);

        // Assert
        Assertions.assertEquals(coordinate, tile.getCoordinate());
    }

    @Test
    void setCoordinateRaw() {
        // Arrange
        tile = new Tile();
        int x = 4;
        int y = 2;
        int z = 1;

        // Act
        tile.setCoordinate(x,y,z);

        // Assert
        Assertions.assertEquals(tile.getCoordinate().getX(), x);
        Assertions.assertEquals(tile.getCoordinate().getY(), y);
        Assertions.assertEquals(tile.getCoordinate().getZ(), z);

    }

    @Test
    void setCoordinateX() {
        // Arrange
        tile = new Tile();
        int x = 22;

        // Act
        tile.setCoordinateX(x);

        // Assert
        Assertions.assertEquals(x, tile.getCoordinate().getX());

    }

    @Test
    void getCoordinateX(){
        // Arrange
        tile = new Tile();
        int x = 44;
        tile.setCoordinateX(x);

        // Act
        int result = tile.getCoordinateX();

        // Assert
        Assertions.assertEquals(x, result);

    }

    @Test
    void setCoordinateY() {
        // Arrange
        tile = new Tile();
        int y = 232;

        // Act
        tile.setCoordinateY(y);

        // Assert
        Assertions.assertEquals(y, tile.getCoordinate().getY());
    }

    @Test
    void getCoordinateY(){
        // Arrange
        tile = new Tile();
        int y = 241;
        tile.setCoordinateY(y);

        // Act
        int result = tile.getCoordinateY();

        // Assert
        Assertions.assertEquals(y, result);

    }

    @Test
    void setName() {
        // Arrange
        tile = new Tile();
        String name = "tree";

        // Act
        tile.setName(name);

        // Assert
        Assertions.assertEquals(name, tile.getName());
    }

    @Test
    void setBodyType() {
        // Arrange
        tile = new Tile();
        String bodyType = "STATIC";

        // Act
        tile.setBodyType(bodyType);

        // Assert
        Assertions.assertEquals(bodyType, tile.getBodyType());

    }

}