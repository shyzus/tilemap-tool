package org.shydev.utility.tilemaptool.model;

import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class LayerTest {

    Layer layer;

    @BeforeEach
    void setUp() {
        layer = new Layer();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addTile() {
        // Arrange
        Tile tile = new Tile(Color.AQUA, 32);

        // Act
        layer.addTile(tile);

        // Assert
        Assertions.assertTrue(layer.getTiles().contains(tile));
    }

    @Test
    void removeTile() {
        // Arrange
        Tile tile = new Tile(Color.AQUA, 32);
        layer.addTile(tile);

        // Act
        layer.removeTile(tile);

        // Assert
        Assertions.assertTrue(layer.getTiles().isEmpty());
    }

    @Test
    void removeTileByCoordinate() {
        // Arrange
        Tile tile = new Tile(Color.AQUA, 32);
        Coordinate coordinate = new Coordinate(24,22,33);
        tile.setCoordinate(coordinate);

        layer.addTile(tile);

        // Act
        layer.removeTileByCoordinate(coordinate);

        // Assert
        Assertions.assertTrue(layer.getTiles().isEmpty());
    }

    @Test
    void removeTileByRow() {
        // Arrange
        Tile tile = new Tile(Color.AQUA, 32);
        int rowIndex = 12;
        tile.setCoordinateY(rowIndex);
        layer.addTile(tile);

        // Act
        layer.removeTileByRow(rowIndex);

        // Assert
        Assertions.assertTrue(layer.getTiles().isEmpty());
    }

    @Test
    void removeTileByColumn() {
        // Arrange
        Tile tile = new Tile(Color.AQUA, 32);
        int columnIndex = 22;
        tile.setCoordinateX(columnIndex);
        layer.addTile(tile);

        // Act
        layer.removeTileByColumn(columnIndex);

        // Assert
        Assertions.assertTrue(layer.getTiles().isEmpty());
    }

    @Test
    void displayLayerNullGridPane() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        layer.getTiles().addAll(Arrays.asList(tiles));

        // Act
        layer.displayLayer();

        // Assert
        Assertions.assertNotNull(layer.getGridPane());
        for (Tile tile : layer.getTiles()) {
            Assertions.assertNotNull(tile.getRectangle());
            Assertions.assertTrue(layer.getGridPane().getChildren().contains(tile.getRectangle()));
        }
    }

    @Test
    void displayLayerNoRectangleTile() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        for (Tile tile : tiles) {
            tile.setRectangle(null);
        }
        layer.getTiles().addAll(Arrays.asList(tiles));

        // Act
        layer.displayLayer();

        // Assert
        Assertions.assertNotNull(layer.getGridPane());
        for (Tile tile : layer.getTiles()) {
            Assertions.assertNotNull(tile.getRectangle());
            Assertions.assertTrue(layer.getGridPane().getChildren().contains(tile.getRectangle()));
        }
    }

    @Test
    void addRow() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        GridPane gridPane = new GridPane();
        int oldRowCount = layer.getRowCount();
        layer.setGridPane(gridPane);

        // Act
        layer.addRow(tiles);

        // Assert
        Assertions.assertTrue(layer.getTiles().containsAll(Arrays.asList(tiles)));
        Assertions.assertEquals(oldRowCount + 1, layer.getRowCount());
    }

    @Test
    void addColumn() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        GridPane gridPane = new GridPane();
        int oldColumnCount = layer.getColumnCount();
        layer.setGridPane(gridPane);

        // Act
        layer.addColumn(tiles);

        // Assert
        Assertions.assertTrue(layer.getTiles().containsAll(Arrays.asList(tiles)));
        Assertions.assertEquals(oldColumnCount + 1, layer.getColumnCount());
    }

    @Test
    void removeRow() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        GridPane gridPane = new GridPane();
        layer.setGridPane(gridPane);
        layer.addRow(tiles);
        int newRowCount = 0;

        // Act
        layer.removeRow(newRowCount);

        // Assert
        Assertions.assertEquals(newRowCount, layer.getRowCount());
        Assertions.assertFalse(layer.getTiles().containsAll(Arrays.asList(tiles)));
    }

    @Test
    void removeColumn() {
        // Arrange
        Tile[] tiles = new Tile[]{new Tile(Color.AQUA, 32), new Tile(Color.RED, 32)};
        GridPane gridPane = new GridPane();
        layer.setGridPane(gridPane);
        layer.addColumn(tiles);
        int newColumnCount = 0;

        // Act
        layer.removeColumn(newColumnCount);

        // Assert
        Assertions.assertEquals(newColumnCount, layer.getColumnCount());
        Assertions.assertFalse(layer.getTiles().containsAll(Arrays.asList(tiles)));
    }

    @Test
    void getName() {
        // Arrange
        String name = "blah";
        layer.setName(name);

        // Act
        String newName = layer.getName();

        // Assert
        Assertions.assertEquals(name, newName);
    }

    @Test
    void setName() {
        // Arrange
        String newName = "blahblah";

        // Act
        layer.setName(newName);

        // Assert
        Assertions.assertEquals(newName, layer.getName());
    }

    @Test
    void toggleGrid() {
        // Arrange
        GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible(false);
        layer.setGridPane(gridPane);

        // Act
        layer.toggleGrid();

        // Assert
        Assertions.assertTrue(gridPane.isGridLinesVisible());
    }
}