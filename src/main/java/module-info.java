module tilemaptool {

    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.swing;
    requires java.logging;
    requires javafx.controls;
    requires gson;
    requires java.sql;
    requires jbox2d.library;

    exports org.shydev.utility.tilemaptool;
    exports org.shydev.utility.tilemaptool.controller;

    opens org.shydev.utility.tilemaptool.controller;
    opens org.shydev.utility.tilemaptool.model;
}