package org.shydev.utility.tilemaptool.model;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Layer implements Serializable {

    private ArrayList<Tile> tiles = new ArrayList<>();
    private transient GridPane gridPane;
    private String name;
    private int rowCount;
    private int columnCount;

    public void addTile(Tile tile) {
        tiles.add(tile);
    }

    public void removeTile(Tile tile) {
        tiles.remove(tile);
    }

    public void removeTileByCoordinate(Coordinate coordinate) {
        tiles.removeIf(tile -> tile.getCoordinate().equals(coordinate));
    }

    public void removeTileByRow(int rowIndex) {
        tiles.removeIf(tile -> tile.getCoordinate().getY() == rowIndex);
    }

    public void removeTileByColumn(int columnIndex) {
        tiles.removeIf(tile -> tile.getCoordinate().getX() == columnIndex);
    }

    public void displayLayer() {

        if (gridPane == null) {
            gridPane = new GridPane();
            gridPane.setGridLinesVisible(true);
        }

        gridPane.getChildren().removeIf(node -> node instanceof Rectangle);

        tiles.forEach(tile -> {
            Coordinate coordinate = tile.getCoordinate();
            if (tile.getRectangle() == null) {
                tile.init();
            }
            gridPane.add(tile.getRectangle(), coordinate.getX(), coordinate.getY());
        });

    }

    public void addRow(Tile[] tiles) {
        for (Tile tile:tiles) {
            addTile(tile);
            Coordinate coordinate = tile.getCoordinate();
            gridPane.add(tile.recreateRectangle(), coordinate.getX(), coordinate.getY());
        }
        setRowCount(gridPane.getRowCount());
    }

    public void addColumn(Tile[] tiles) {
        for (Tile tile:tiles) {
            addTile(tile);
            Coordinate coordinate = tile.getCoordinate();
            gridPane.add(tile.recreateRectangle(), coordinate.getX(), coordinate.getY());
        }
        setColumnCount(gridPane.getColumnCount());
    }

    public void removeRow(int newRowCount) {
        Iterator<Node> nodeIterator = getGridPane().getChildren().iterator();

        while (nodeIterator.hasNext()) {
            Node node = nodeIterator.next();

            if (node instanceof Rectangle) {
                int nodeRowIndex = GridPane.getRowIndex(node);
                if (nodeRowIndex >= newRowCount) {
                    nodeIterator.remove();
                    removeTileByRow(nodeRowIndex);
                }
            }
        }

        setRowCount(gridPane.getRowCount());
    }

    public void removeColumn(int newColumnCount) {
        Iterator<Node> nodeIterator = getGridPane().getChildren().iterator();

        while (nodeIterator.hasNext()) {
            Node node = nodeIterator.next();

            if (node instanceof Rectangle) {
                int nodeColumnIndex = GridPane.getColumnIndex(node);
                if (nodeColumnIndex >= newColumnCount) {
                    nodeIterator.remove();
                    removeTileByColumn(nodeColumnIndex);
                }
            }
        }
        setColumnCount(gridPane.getColumnCount());
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public void setGridPane(GridPane gridPane) {
        this.gridPane = gridPane;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void toggleGrid() {
        gridPane.setGridLinesVisible(!gridPane.isGridLinesVisible());
    }
}
