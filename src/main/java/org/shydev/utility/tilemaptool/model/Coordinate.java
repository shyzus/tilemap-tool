package org.shydev.utility.tilemaptool.model;

import java.io.Serializable;

/**
 * Coordinate class that represents the coordinates of an object on the map.
 *
 * @author shyzus <dev@shyzus.com>
 */
public class Coordinate implements Serializable {

    private int x;
    private int y;
    private int z;

    /**
     * Create a coordinate.
     *
     * @param x X-Coordinate
     * @param y Y-Coordinate
     */
    public Coordinate(int x, int y, int z) {
       this.x = x;
       this.y = y;
       this.z = z;
    }

    /**
     * Get X-Coordinate
     *
     * @return X-Coordinate as primitive integer
     */
    public int getX() {
        return x;
    }

    /**
     * Set X-Coordinate
     *
     * @param x X-Coordinate as primitive integer
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Get Y-Coordinate
     *
     * @return Y-Coordinate as primitive integer
     */
    public int getY() {
        return y;
    }

    /**
     * Set Y-Coordinate
     *
     * @param y Y-Coordinate as primitive integer
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Get Z-Coordinate
     *
     * @return Z-Coordinate as primitive integer
     */
    public int getZ() {
        return z;
    }

    /**
     * Set Z-Coordinate
     *
     * @param z Z-Coordinate as primitive integer
     */
    public void setZ(int z) {
        this.z = z;
    }
    /**
     * Set X and Y Coordinates
     *
     * @param x X-Coordinate as primitive integer
     * @param y Y-Coordinate as primitive integer
     */
    public void setCoordinate(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Coordinate) {
            return getX() == ((Coordinate) obj).getX() &&
                    getY() == ((Coordinate) obj).getY() &&
                    getZ() == ((Coordinate) obj).getZ();
        } else {
            return false;
        }
    }
}
