package org.shydev.utility.tilemaptool.model;

import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import org.shydev.utility.tilemaptool.service.Base64Service;
import org.shydev.utility.tilemaptool.service.TileMapService;

import java.io.Serializable;

/**
 * Tile class represents a single tile on a map.
 *
 * @author shyzus <dev@shyzus.com>
 */
public class Tile implements Serializable {

    // TODO: Fetch this come from properties
    private final int TILE_SIZE = 32;

    private String name;
    private transient Rectangle rectangle;
    private transient Paint baseColor;
    private String colorHex;
    private Coordinate coordinate;
    private String bodyType;
    private int base64Index;

    /**
     * No-args constructor
     */
    public Tile() {
        coordinate = new Coordinate(0, 0, 0);
    }

    /**
     * Constructor using image without name.
     *
     * @param base64Index Index of the image in the base64 array
     * @param size        Size to set rectanglegetClass().getResource("/test-tilemap.json").getPath()
     */
    public Tile(int base64Index, int size) {
        rectangle = new Rectangle(size, size, new ImagePattern(
                Base64Service.decode(
                        TileMapService.getLoadedTileMap().getBase64Tiles().get(base64Index)
                )
        ));
        this.base64Index = base64Index;
        coordinate = new Coordinate(0, 0, 0);
    }

    /**
     * Constructor using abstract Paint class without name.
     *
     * @param baseColor Object that extends Paint
     * @param size      Size to set rectangle
     * @see Paint
     * @see Color
     */
    public Tile(Paint baseColor, int size) {
        this.baseColor = baseColor;
        name = colorHex = baseColor.toString();
        base64Index = -1;
        rectangle = new Rectangle(size, size, baseColor);
        coordinate = new Coordinate(0, 0, 0);
    }

    /**
     * Constructor using image and name.
     *
     * @param base64Index Index of the image in the base64 array
     * @param name        Name of image/tile
     * @param size        Size to set rectangle
     */
    public Tile(int base64Index, String name, int size) {
        this(base64Index, size);
        this.name = name;
    }

    /**
     * Constructor using abstract Paint class with name.
     *
     * @param baseColor Object that extends Paint
     * @param name      Name of tile/image
     * @param size      Size to set rectangle
     * @see Paint
     * @see Color
     */
    public Tile(Paint baseColor, String name, int size) {
        this(baseColor, size);
        this.name = name;
        base64Index = -1;
        rectangle = new Rectangle(size, size, baseColor);
        coordinate = new Coordinate(0, 0, 0);
    }

    /**
     * Initialisation method to be used once after serialisation.
     */
    public void init() {
        if (rectangle == null || baseColor == null) {
            if (colorHex != null) {
                baseColor = Color.valueOf(colorHex);
                rectangle = new Rectangle(TILE_SIZE, TILE_SIZE, Color.valueOf(colorHex));
            } else {
                rectangle = new Rectangle(TILE_SIZE, TILE_SIZE,
                        new ImagePattern(
                                Base64Service.decode(
                                        TileMapService.getLoadedTileMap().getBase64Tiles().get(base64Index)
                                )
                        )
                );
            }
        }
    }

    public Rectangle recreateRectangle() {
        Rectangle rectangleToReturn;

        if (baseColor == null) {
            rectangleToReturn = new Rectangle(TILE_SIZE, TILE_SIZE,
                    new ImagePattern(
                            Base64Service.decode(
                                    TileMapService.getLoadedTileMap().getBase64Tiles().get(base64Index)
                            )
                    )
            );
        } else {
            rectangleToReturn = new Rectangle(TILE_SIZE, TILE_SIZE, baseColor);
        }
        setRectangle(rectangleToReturn);
        return rectangleToReturn;
    }

    /**
     * Get Coordinate object
     *
     * @return Coordinate
     * @see Coordinate
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     * Set Coordinate values
     *
     * @param x X-Coordinate
     * @param y Y-Coordinate
     */
    public void setCoordinate(int x, int y, int z) {
        coordinate.setCoordinate(x, y, z);
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * Set only X-Coordinate
     *
     * @param x X-Coordinate
     */
    public void setCoordinateX(int x) {
        coordinate.setX(x);
    }

    public int getCoordinateX() {
        return coordinate.getX();
    }

    /**
     * Set only Y-Coordinate
     *
     * @param y Y-Coordinate
     */
    public void setCoordinateY(int y) {
        coordinate.setY(y);
    }

    public int getCoordinateY() {
        return getCoordinate().getY();
    }

    /**
     * Get Paint object
     *
     * @return Paint object
     */
    public Paint getBaseColor() {
        return baseColor;
    }

    /**
     * Get hex value representing color
     *
     * @return Hex value
     */
    public String getColorHex() {
        return colorHex;
    }

    /**
     * Get rectangle object
     *
     * @return rectangle
     */
    public Rectangle getRectangle() {
        return rectangle;
    }

    /**
     * Set rectangle object
     *
     * @param rectangle rectangle
     */
    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    /**
     * Set Paint object
     *
     * @param baseColor Paint object
     */
    public void setBaseColor(Paint baseColor) {
        this.baseColor = baseColor;
    }

    /**
     * Set hex value representing color
     *
     * @param colorHex Hex value
     */
    public void setColorHex(String colorHex) {
        this.colorHex = colorHex;
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public int getbase64Index() {
        return base64Index;
    }

    public void setBase64Index(int base64Index) {
        this.base64Index = base64Index;
    }


}
