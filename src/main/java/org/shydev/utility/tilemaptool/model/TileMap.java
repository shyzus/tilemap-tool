package org.shydev.utility.tilemaptool.model;

import javafx.scene.layout.Pane;

import java.io.Serializable;
import java.util.ArrayList;

public class TileMap implements Serializable {

    private ArrayList<Layer> layerList = new ArrayList<>();
    private ArrayList<String> base64Tiles = new ArrayList<>();
    private Layer topLayer = null;

    public void init() {
        layerList.forEach(layer -> {
            layer.displayLayer();
            topLayer.displayLayer();
            setTopLayer(topLayer);
        });

    }

    public void addTileMapToPane(Pane pane) {
        for (Layer layer:layerList) {
            pane.getChildren().add(layer.getGridPane());
        }
    }

    public Layer getLayerByName(String name) {
        for (Layer layer : layerList) {
            if (layer.getName().equals(name)) {
                return layer;
            }
        }
        return null;
    }

    public void addLayerToPane(Layer layer, Pane pane) {
        layerList.add(layer);
        if (layer.getGridPane() == null) {
            layer.displayLayer();
        }
        pane.getChildren().add(layer.getGridPane());
    }

    public void removeLayerFromPane(Layer layer, Pane pane) {
        layerList.remove(layer);
        pane.getChildren().remove(layer.getGridPane());
    }

    public Layer getTopLayer() {
        return topLayer;
    }

    public void setTopLayer(Layer topLayer) {
        this.topLayer = topLayer;
        if (topLayer != null && topLayer.getGridPane() != null) {
            topLayer.getGridPane().toFront();
        }
    }

    public void addColumn(Tile[] tiles) {
        layerList.forEach(layer -> layer.addColumn(tiles));
    }

    public void addRow(Tile[] tiles) {
        layerList.forEach(layer -> layer.addRow(tiles));
    }

    public void removeColumn(int newColumnCount) {
        layerList.forEach(layer -> layer.removeColumn(newColumnCount));
    }

    public void removeRow(int newRowCount) {
        layerList.forEach(layer -> layer.removeRow(newRowCount));
    }

    public String[] getLayerNames() {
        String[] layerNames = new String[layerList.size()];
        for (int i = 0; i < layerList.size(); i++) {
            layerNames[i] = layerList.get(i).getName();
        }
       return layerNames;
    }

    public ArrayList<Layer> getLayerList() {
        return layerList;
    }

    public void toggleGrid() {
        layerList.forEach(Layer::toggleGrid);
    }

    public ArrayList<String> getBase64Tiles() {
        return base64Tiles;
    }

    public void setBase64Tiles(ArrayList<String> base64Tiles) {
        this.base64Tiles = base64Tiles;
    }
}
