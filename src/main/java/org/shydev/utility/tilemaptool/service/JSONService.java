package org.shydev.utility.tilemaptool.service;

import com.google.gson.Gson;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JSONService {

    private Gson gson;

    public JSONService() {
        gson = new Gson();
    }

    public Object jsonToObject(String json, Class<?> objectClass) {
        return gson.fromJson(json, objectClass);
    }

    public Object readFromFileToObject(String url, Class<?> objectClass) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(url));
        Object object = gson.fromJson(reader, objectClass);

        Logger.getGlobal().log(Level.INFO, "Read map successfully: ".concat(url));
        return object;
    }

    public Object readFromFileToObject(File file, Class<?> objectClass) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        Object object = gson.fromJson(reader, objectClass);

        Logger.getGlobal().log(Level.INFO, "Read map successfully: ".concat(file.getPath()));
        return object;
    }

    private void directoryCheck(String path) throws IOException {
        if (!Files.exists(Path.of(path)) || !Files.isDirectory(Path.of(path))) {
            Files.createDirectory(Path.of(path));
        }
    }

    public void writeJSONToFile(String path, String fileName, String json) throws IOException {
        if (!path.endsWith("/")) {
            path = path.concat("/");
        }

        directoryCheck(path);

        fileName = fileName.concat(".json");

        FileWriter writer = new FileWriter(path.concat(fileName));
        writer.write(json);
        writer.close();
    }

    public void writeObjectToFile(String path, String fileName, Object object) throws IOException {
        if (!path.endsWith("/")) {
            path = path.concat("/");
        }

        directoryCheck(path);

        fileName = fileName.concat(".json");

        FileWriter writer = new FileWriter(path.concat(fileName));
        writer.write(gson.toJson(object));
        writer.close();
        Logger.getGlobal().log(Level.INFO, "Saved map successfully: ".concat(path.concat(fileName)));
    }
}
