package org.shydev.utility.tilemaptool.service;

import javafx.scene.image.Image;

import java.io.*;
import java.util.Base64;

public class Base64Service {

    public static String encode(String imagePath) {
        String base64Image = "";
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            byte[] imageData = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
        } catch (FileNotFoundException e) {
            System.out.println("Image not found: " + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        return base64Image;
    }

    public static Image decode(String base64String) {
        byte[] imageByteArray = Base64.getDecoder().decode(base64String);
        return new Image(new ByteArrayInputStream(imageByteArray));

//        try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
//            // Converting a Base64 String into Image byte array
//            byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
//            //imageOutFile.write(imageByteArray);
//            return new Image(new ByteArrayInputStream(imageByteArray));
//        } catch (FileNotFoundException e) {
//            System.out.println("Image not found" + e);
//        } catch (IOException ioe) {
//            System.out.println("Exception while reading the Image " + ioe);
//        }
    }
}
