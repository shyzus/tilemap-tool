package org.shydev.utility.tilemaptool.service;

import org.shydev.utility.tilemaptool.model.TileMap;

public class TileMapService {

    static TileMap loadedTileMap;

    private TileMapService() {

    }

    public static void setLoadedTileMap(TileMap tileMap) {
        loadedTileMap = tileMap;
    }

    public static TileMap getLoadedTileMap() {
        return loadedTileMap;
    }
}
