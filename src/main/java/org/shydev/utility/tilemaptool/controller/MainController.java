package org.shydev.utility.tilemaptool.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import org.jbox2d.dynamics.BodyType;
import org.shydev.utility.tilemaptool.model.Coordinate;
import org.shydev.utility.tilemaptool.model.Layer;
import org.shydev.utility.tilemaptool.model.Tile;
import org.shydev.utility.tilemaptool.model.TileMap;
import org.shydev.utility.tilemaptool.service.Base64Service;
import org.shydev.utility.tilemaptool.service.JSONService;
import org.shydev.utility.tilemaptool.service.TileMapService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController implements Initializable {

    @FXML
    private AnchorPane masterPane;

    @FXML
    private AnchorPane mainContentPane;

    @FXML
    private AnchorPane detailsPane;

    @FXML
    private ScrollPane mainContentScrollPane;

    @FXML
    private TextField rowCountField;

    @FXML
    private TextField columnCountField;

    @FXML
    private TilePane iconPane;

    @FXML
    private ListView<Node> layerListView;

    @FXML
    private ChoiceBox<String> topLayerChoiceBox;

    @FXML
    private ChoiceBox<String> bodyTypeChoiceBox;

    private StackPane layerStackPane = new StackPane();

    private JSONService jsonService = new JSONService();

    private TileMap tileMap = new TileMap();

    private ImageView selectedImageView;

    private final int TILE_SIZE = 32;
    private final int MAX_FIELD_CONTENT_LENGTH = 4;
 //   private final String SAVE_DIRECTORY = System.getProperty("user.home").concat("/.tilemaptool/");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //checkEnvironment();

        TileMapService.setLoadedTileMap(tileMap);

        tileMap.init();

        configureInputs();

        topLayerChoiceBox.setOnAction(event -> {
            if (topLayerChoiceBox.getValue() != null && !topLayerChoiceBox.getValue().isBlank()) {
                tileMap.setTopLayer(tileMap.getLayerByName(topLayerChoiceBox.getValue()));
            }

        });

        addLayer(null);
        addRow(null);
        addColumn(null);

        mainContentPane.getChildren().add(layerStackPane);
    }

    @FXML
    void save(ActionEvent event) {

        File file = selectSaveFile();

        String filename = file.getName().split("\\.")[0];

        try {
            jsonService.writeObjectToFile(file.getParent(), filename, tileMap);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage());
        }
    }

    @FXML
    void open(ActionEvent event) {

        File selectedFile = selectMapFile();

        if (selectedFile != null) {
            layerStackPane.getChildren().clear();

            try {
                tileMap = (TileMap) jsonService.readFromFileToObject(selectedFile, TileMap.class);
                TileMapService.setLoadedTileMap(tileMap);
                tileMap.init();
                layerListView.getItems().clear();
                layerStackPane.getChildren().clear();
                iconPane.getChildren().clear();
                loadIconFromTileMap(tileMap);
                tileMap.addTileMapToPane(layerStackPane);
                tileMap.getLayerList().forEach(layer -> {
                    layer.getTiles().forEach(Tile::init);
                    addLayerEntry(layer);
                });

                columnCountField.setText(String.valueOf(tileMap.getTopLayer().getColumnCount()));
                rowCountField.setText(String.valueOf(tileMap.getTopLayer().getRowCount()));

            } catch (FileNotFoundException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage());
            }
        }

    }

    @FXML
    void addIcon(ActionEvent event) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image Files");
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("Image files", "*.png", "*.jpg", "*.jpeg"));

        List<File> files = fileChooser.showOpenMultipleDialog(masterPane.getScene().getWindow());

        if (files != null) {
            files.forEach(file -> {
                loadIconFromFile(file, event);
            });
        }
    }

    private void loadIconFromTileMap(TileMap tileMap) {
        tileMap.getBase64Tiles().forEach(base64 -> {
            ImageView imageView = new ImageView(Base64Service.decode(base64));

            imageView.setFitHeight(32);
            imageView.setFitWidth(32);

            imageView.setUserData(tileMap.getBase64Tiles().indexOf(base64));

            imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
                if (mouseEvent.getButton() == MouseButton.PRIMARY) {
                    // Set url in userdata. url of image is null. Reason unknown
                    selectedImageView = imageView;
                    mouseEvent.consume();
                } else if(mouseEvent.getButton() == MouseButton.SECONDARY) {
                    iconPane.getChildren().remove(imageView);
                    mouseEvent.consume();
                }

            });

            //Tooltip.install(imageView, new Tooltip(file.getName()));

            iconPane.getChildren().add(imageView);
        });
    }

    private void loadIconFromFile(File file, ActionEvent event) {
        for (Node imageView1 : iconPane.getChildren()) {
            if (imageView1 instanceof ImageView &&
                    imageView1
                            .getUserData()
                            .equals(tileMap.getBase64Tiles().indexOf(Base64Service.encode(file.toURI().toString())))
            )
            {
                return;
            }
        }

        String base64String = Base64Service.encode(file.getPath());

        if (!tileMap.getBase64Tiles().contains(base64String)){
            tileMap.getBase64Tiles().add(base64String);
        }

        ImageView imageView = new ImageView(Base64Service.decode(base64String));

        imageView.setFitHeight(32);
        imageView.setFitWidth(32);

        imageView.setUserData(tileMap.getBase64Tiles().indexOf(base64String));

        imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (mouseEvent.getButton() == MouseButton.PRIMARY) {
                // Set url in userdata. url of image is null. Reason unknown
                selectedImageView = imageView;
                event.consume();
            } else if(mouseEvent.getButton() == MouseButton.SECONDARY) {
                iconPane.getChildren().remove(imageView);
                event.consume();
            }

        });

        Tooltip.install(imageView, new Tooltip(file.getName()));

        iconPane.getChildren().add(imageView);
    }

    @FXML
    void addColumn(ActionEvent event) {

        Tile[] tiles;
        Layer topLayer = tileMap.getTopLayer();

        if (topLayer != null) {
            if (topLayer.getRowCount() == 0) {
                tiles = new Tile[1];
            } else {
                tiles = new Tile[topLayer.getRowCount()];
            }


            for (int i = 0; i < tiles.length; i++) {
                Tile tile = new Tile(Color.TRANSPARENT, TILE_SIZE);
                tile.setCoordinate(topLayer.getColumnCount(), i, tileMap.getLayerList().indexOf(topLayer));
                tiles[i] = tile;
            }
            tileMap.addColumn(tiles);
            columnCountField.setText(String.valueOf(topLayer.getColumnCount()));
        }

    }

    @FXML
    void addRow(ActionEvent event) {

        Tile[] tiles;
        Layer topLayer = tileMap.getTopLayer();

        if (topLayer != null) {
            if (topLayer.getColumnCount() == 0) {
                tiles = new Tile[1];
            } else {
                tiles = new Tile[topLayer.getColumnCount()];
            }

            for (int i = 0; i < tiles.length; i++) {
                Tile tile = new Tile(Color.TRANSPARENT, TILE_SIZE);
                tile.setCoordinate(i, topLayer.getRowCount(), tileMap.getLayerList().indexOf(topLayer));
                tiles[i] = tile;
            }

            tileMap.addRow(tiles);
            rowCountField.setText(String.valueOf(topLayer.getRowCount()));

        }
    }

    private void addLayerEntry(Layer layer) {

        RadioButton radioButton = new RadioButton(layer.getName());
        radioButton.setSelected(true);
        radioButton.setOnAction(actionEvent -> {
            layer.getGridPane().setVisible(!layer.getGridPane().isVisible());
        });

        layer.getGridPane().setGridLinesVisible(true);

        Button button = new Button("X");
        button.setOnAction(event1 -> {
            tileMap.removeLayerFromPane(layer, layerStackPane);
            layerListView.getItems().removeIf(node -> node instanceof HBox && node.getUserData().equals(layer.getName()));
            //layerVBox.getChildren().removeIf(node -> node instanceof HBox && node.getUserData().equals(layer.getName()));
            topLayerChoiceBox.getItems().setAll(tileMap.getLayerNames());
            if (tileMap.getTopLayer() != null && tileMap.getTopLayer().getName() != null) {
                topLayerChoiceBox.setValue(tileMap.getTopLayer().getName());
            } else {
                topLayerChoiceBox.setValue("");
            }
        });


        HBox hBox = new HBox(radioButton, button);
        hBox.setUserData(layer.getName());
        layerListView.getItems().add(hBox);
        //layerVBox.getChildren().add(hBox);

        GridPane gridPane = layer.getGridPane();
        if (gridPane.getOnMouseClicked() == null && gridPane.getOnMouseDragged() == null) {
            EventHandler<MouseEvent> eventHandler = (MouseEvent mouseEvent) -> {

                Node clickedNode = mouseEvent.getPickResult().getIntersectedNode();

                switch (mouseEvent.getButton()) {
                    case PRIMARY:
                        if (clickedNode instanceof Rectangle && selectedImageView != null) {

                            Rectangle rectangleNode = (Rectangle) clickedNode;
                            ImagePattern imagePattern = new ImagePattern(selectedImageView.getImage());

                            if (rectangleNode.getFill().equals(Color.TRANSPARENT) ||
                                    !((ImagePattern) rectangleNode.getFill()).getImage().equals(imagePattern.getImage())) {
                                rectangleNode.setFill(imagePattern);
                                // Use userdata. image url is null trough image object. reason unknown
                                Tile tile = new Tile((int)selectedImageView.getUserData(), TILE_SIZE);
                                tile.setBodyType(bodyTypeChoiceBox.getValue());
                                tile.setCoordinate(GridPane.getColumnIndex(clickedNode), GridPane.getRowIndex(clickedNode), tileMap.getLayerList().indexOf(layer));
                                layer.addTile(tile);
                            }
                        }
                        break;
                    case SECONDARY:
                        if (clickedNode instanceof Rectangle) {
                            Rectangle rectangleNode = (Rectangle) clickedNode;
                            if (!rectangleNode.getFill().equals(Color.TRANSPARENT)) {
                                layer.removeTileByCoordinate(new Coordinate(GridPane.getColumnIndex(clickedNode), GridPane.getRowIndex(clickedNode), tileMap.getLayerList().indexOf(layer)));
                                rectangleNode.setFill(Color.TRANSPARENT);
                            }
                        }
                        break;
                }

            };

            gridPane.setOnMouseClicked(eventHandler);
            gridPane.setOnMouseDragged(eventHandler);
        }

        topLayerChoiceBox.getItems().setAll(tileMap.getLayerNames());
        topLayerChoiceBox.setValue(layer.getName());
    }

    @FXML
    void addLayer(ActionEvent event) {
        Layer layer = new Layer();
        GridPane gridPane = new GridPane();
        layer.setGridPane(gridPane);
        String name = "Layer ".concat(String.valueOf(layerListView.getItems().size() + 1));
        layer.setName(name);

        Layer previousTopLayer = tileMap.getTopLayer();

        tileMap.addLayerToPane(layer, layerStackPane);

        addLayerEntry(layer);

        if (previousTopLayer != null) {
            for (int i = 0; i < previousTopLayer.getColumnCount(); i++) {
                addColumn(null);
            }
            for (int i = 0; i < previousTopLayer.getRowCount(); i++) {
                addRow(null);
            }

        }
    }

    @FXML
    void updateColumnCount(ActionEvent event) {
        try {
            int newColumnCount = Integer.parseInt(columnCountField.getText());

            if (newColumnCount < 0) {
                Logger.getGlobal().log(Level.WARNING, "Cannot input negative integer!");
                return;
            }

            Layer topLayer = tileMap.getTopLayer();
            if (topLayer != null) {
                int currentColumnCount = topLayer.getColumnCount();

                if (newColumnCount > currentColumnCount) {
                    for (int i = 0; i < newColumnCount - currentColumnCount; i++) {
                        addColumn(event);
                    }
                } else {
                    tileMap.removeColumn(newColumnCount);
                }
            }
        } catch (NumberFormatException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage());
        }
    }

    @FXML
    void updateRowCount(ActionEvent event) {
        try {
            int newRowCount = Integer.parseInt(rowCountField.getText());

            if (newRowCount < 0) {
                Logger.getGlobal().log(Level.WARNING, "Cannot input negative integer!");
                return;
            }

            Layer topLayer = tileMap.getTopLayer();

            if (topLayer != null) {
                int currentRowCount = topLayer.getRowCount();

                if (newRowCount > currentRowCount) {
                    for (int i = 0; i < newRowCount - currentRowCount; i++) {
                        addRow(event);
                    }
                } else {
                    tileMap.removeRow(newRowCount);
                }
            }
        } catch (NumberFormatException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage());
        }
    }

    private void configureInputs() {

       layerListView.setMinSize(300,30);
       layerListView.setPrefSize(300, 250);

        // Limit inputs
        EventHandler<KeyEvent> maxLengthEventHandler = (KeyEvent keyEvent) -> {
            if (keyEvent.getSource() instanceof TextField) {
                TextField textField = (TextField) keyEvent.getSource();
                if (textField.getText().length() > MAX_FIELD_CONTENT_LENGTH) {
                    textField.deleteText(MAX_FIELD_CONTENT_LENGTH, MAX_FIELD_CONTENT_LENGTH + 1);
                }
            }
        };

        rowCountField.setOnKeyTyped(maxLengthEventHandler);
        columnCountField.setOnKeyTyped(maxLengthEventHandler);

        bodyTypeChoiceBox.getItems().add("None");
        bodyTypeChoiceBox.setValue("None");

        for (BodyType bodyType : BodyType.values()) {
            bodyTypeChoiceBox.getItems().add(bodyType.name());
        }
    }

    private File selectMapFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.setTitle("Open Map File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files", "*.json"));

        return fileChooser.showOpenDialog(masterPane.getScene().getWindow());
    }

    private File selectSaveFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.setTitle("Save Map File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files", "*.json"));

        return fileChooser.showSaveDialog(masterPane.getScene().getWindow());
    }

    @FXML
    public void quit(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    public void toggleGrid(ActionEvent event) {
        tileMap.toggleGrid();
    }
}
